package com.gitee.jwds666.fs.utils;

public class FileType {

    String name;
    String icon;
    boolean openFlag;
    String httpHeader;


    public FileType(String name, String icon, boolean openFlag) {
        this.name = name;
        this.icon = icon;
        this.openFlag = openFlag;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isOpenFlag() {
        return openFlag;
    }

    public void setOpenFlag(boolean openFlag) {
        this.openFlag = openFlag;
    }
}
