package com.gitee.jwds666.fs.account;

import com.gitee.jwds666.fs.account.permission.Permission;
import com.gitee.jwds666.fs.account.permission.PermissionResolver;

/**
 * 账户
 */
public interface Account {

    /**
     * 获取账户标识
     * @return
     */
    String getAccount();

    /**
     * 是否包含权限
     * @param permission
     * @return
     */
    boolean includes(String permission,PermissionResolver resolver);

    /**
     * 是否包含权限
     * @param permission
     * @return
     */
    boolean includes(Permission permission);


    /**
     * 是否包含此权限的查看操作
     * @param permission
     * @return
     */
    boolean includeSelect(String permission,PermissionResolver resolver);

    /**
     * 是否包含此权限的添加操作
     * @param permission
     * @return
     */
    boolean includeInsert(String permission,PermissionResolver resolver);

    /**
     * 是否包含此权限的修改操作
     * @param permission
     * @return
     */
    boolean includeUpdate(String permission,PermissionResolver resolver);

    /**
     * 是否包含此权限的删除操作
     * @param permission
     * @return
     */
    boolean includeDelete(String permission,PermissionResolver resolver);
}
