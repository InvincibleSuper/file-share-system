package com.gitee.jwds666.fs.account;

import com.gitee.jwds666.fs.env.Refresh;

/**
 * 账户管理器
 */
public interface AccountManager extends Refresh {

    /**
     * 获取一个账户
     * @param token
     * @return
     */
    Account get(String token);


    /**
     * 添加一个账户
     * @param account
     * @return
     */
    Account add(Account account);

}
