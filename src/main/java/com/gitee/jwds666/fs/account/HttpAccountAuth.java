package com.gitee.jwds666.fs.account;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public abstract class HttpAccountAuth extends AbstractAccountAuth {


    protected HttpServletRequest getRequest(){
        return ((ServletRequestAttributes)(RequestContextHolder.getRequestAttributes())).getRequest();
    }


}
