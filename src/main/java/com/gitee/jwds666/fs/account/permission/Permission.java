package com.gitee.jwds666.fs.account.permission;


import org.springframework.util.AntPathMatcher;

public class Permission {
    public static String pathSeparator = "/";
    public static AntPathMatcher antPathMatcher = new AntPathMatcher(pathSeparator);
    private String dirPermission;

    private String [] operationPermission;

    public String getDirPermission() {
        return dirPermission;
    }

    public void setDirPermission(String dirPermission) {
        this.dirPermission = dirPermission;
    }

    public String[] getOperationPermission() {
        return operationPermission;
    }

    public void setOperationPermission(String[] operationPermission) {
        this.operationPermission = operationPermission;
    }

    public Permission() {
    }

    public boolean include(Permission permissionObj){
        if (!search(operationPermission,"*")){
            for (String s : permissionObj.getOperationPermission()) {
                if (!search(operationPermission,s))return false;
            }
        }
        return antPathMatcher.match(dirPermission,permissionObj.getDirPermission());
    }


    private boolean search(String [] a1 ,String searchStr){
        for (String s : a1) {
            if (s.equals(searchStr))return true;
        }
        return false;
    }
}
