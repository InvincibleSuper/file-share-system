package com.gitee.jwds666.fs.account.permission;

public class PathPermissionResolver extends DefaultPermissionResolver {
    @Override
    public Permission resolve(String permission) {
        permission = permission.replace("\\","/");
        if (permission.charAt(permission.length() - 1) == '/'){
            permission += "**";
        }
        return super.resolve(permission);
    }
}
