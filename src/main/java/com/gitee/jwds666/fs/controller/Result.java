package com.gitee.jwds666.fs.controller;

public class Result {


    private int code;


    private Object msg;


    private Object data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    public static Result success(){
        return success(null);
    }
    public static Result success(Object data){
        Result res = new Result();
        res.code = 200;
        res.msg="成功";
        res.data = data;
        return res;
    }

    public static Result error(Object msg){
        Result res = new Result();
        res.code = 500;
        res.msg=msg;
        res.data = null;
        return res;
    }

}
