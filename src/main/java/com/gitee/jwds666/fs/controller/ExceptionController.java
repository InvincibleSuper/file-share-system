package com.gitee.jwds666.fs.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {


    @ExceptionHandler(RuntimeException.class)
    public Result runtimeException(RuntimeException e){
        e.printStackTrace();
        return Result.error(e.getMessage());
    }


}
