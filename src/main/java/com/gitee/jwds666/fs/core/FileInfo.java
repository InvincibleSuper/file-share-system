package com.gitee.jwds666.fs.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.File;
import java.util.Date;

/**
 * 文件信息
 */
public class FileInfo {


    /**
     * 名称
     */
    private String name;

    /**
     * 路径
     */
    private String path;

    /**
     * 是否是文件
     */
    private boolean isFile;


    /**
     * 是否存在
     */
    private boolean exists;

    /**
     * 后缀
     */
    private String suffix;

    /**
     * 文件类别
     */
    private String fileType;

    /**
     * 图标
     */
    private String icon;

    /**
     * 是否能在网页打开
     */
    private boolean openFlag;

    /**
     * 大小 byte
     */
    private long size;

    /**
     * 是否可以查看
     */
    private boolean select;

    /**
     * 是否可以增加
     */
    private boolean insert;

    /**
     * 是否可以修改
     */
    private boolean update;

    /**
     * 是否可以删除
     */
    private boolean delete;


    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @JsonIgnore
    private File file;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public boolean isOpenFlag() {
        return openFlag;
    }

    public void setOpenFlag(boolean openFlag) {
        this.openFlag = openFlag;
    }

    public boolean getIsFile() {
        return isFile;
    }

    public void setIsFile(boolean file) {
        isFile = file;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public boolean isInsert() {
        return insert;
    }

    public void setInsert(boolean insert) {
        this.insert = insert;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }



    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
