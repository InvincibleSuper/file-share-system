package com.gitee.jwds666.fs.core;

import java.io.File;
import java.util.List;

public interface FileConvertFileInfo {

    /**
     * 文件转换文件信息
     * @param file
     * @return
     */
    FileInfo convert(File file);

    /**
     * 获取文件转换为文件信息
     * @param path
     * @return
     */
    FileInfo convert(String path);

    /**
     * 转换列表
     * @param files
     * @return
     */
    List<FileInfo> convertList(List<File> files);




}
