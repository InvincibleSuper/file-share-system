package com.gitee.jwds666.fs.core;

import com.gitee.jwds666.fs.account.AccountAuth;
import com.gitee.jwds666.fs.config.FileShareConfig;
import com.gitee.jwds666.fs.env.FileConstants;
import com.gitee.jwds666.fs.utils.FileTypeUtils;
import com.gitee.jwds666.fs.utils.FileUtils;
import com.gitee.jwds666.fs.account.permission.PermissionResolver;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DefaultFileConvertFileInfo implements FileConvertFileInfo {


    private AccountAuth accountAuth;

    private PermissionResolver permissionResolver;

    private FileShareConfig config;

    private Comparator<FileInfo> comparator;


    public DefaultFileConvertFileInfo(AccountAuth accountAuth, PermissionResolver permissionResolver,FileShareConfig config,Comparator<FileInfo> comparator) {
        this.accountAuth = accountAuth;
        this.permissionResolver = permissionResolver;
        this.config = config;
        this.comparator = comparator;
    }

    /**
     * 文件转换文件信息
     *
     * @param file
     * @return
     */
    @Override
    public FileInfo convert(File file) {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setFile(file);
        fileInfo.setName(file.getName());
        fileInfo.setPath(file.getPath().substring(config.getBasePath().length()-1).replace(FileConstants.SEPARATOR,"/"));
        if (fileInfo.getPath().length() == 0 || fileInfo.getPath().charAt(0) != '/'){
            fileInfo.setPath('/'+fileInfo.getPath());
        }
        fileInfo.setExists(file.exists());
        if (fileInfo.isExists()){
            fileInfo.setIsFile(file.isFile());
            fileInfo.setFileType(FileTypeUtils.getFileType(file));
            fileInfo.setIcon(FileTypeUtils.getFileTypeIcon(file));
            fileInfo.setOpenFlag(FileTypeUtils.getFileOpenFlag(file));
            fileInfo.setUpdateTime(FileUtils.getFileUpdateTime(file));
            fileInfo.setSuffix(FileUtils.getFileSuffix(file));
            fileInfo.setSize(file.length());
        }
        fileInfo.setSelect(accountAuth.auth().includeSelect(fileInfo.getPath(),permissionResolver));
        fileInfo.setInsert(accountAuth.auth().includeInsert(fileInfo.getPath(),permissionResolver));
        fileInfo.setUpdate(accountAuth.auth().includeUpdate(fileInfo.getPath(),permissionResolver));
        fileInfo.setDelete(accountAuth.auth().includeDelete(fileInfo.getPath(),permissionResolver));
        return fileInfo;
    }

    /**
     * 获取文件转换为文件信息
     *
     * @param path
     * @return
     */
    @Override
    public FileInfo convert(String path) {
        return convert(new File(path));
    }

    /**
     * 转换列表
     *
     * @param files
     * @return
     */
    @Override
    public List<FileInfo> convertList(List<File> files) {
        List<FileInfo> res = new ArrayList<>();
        for (File file : files) {
            res.add(convert(file));
        }
        Collections.sort(res,comparator);
        return res;
    }
}
