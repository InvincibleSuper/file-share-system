package com.gitee.jwds666.fs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("share")
@Component
public class FileShareConfig {


    private String basePath;

    private String version;


    private String git;


    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
