package com.gitee.jwds666.fs.env;

import com.gitee.jwds666.fs.account.AccountManager;
import com.gitee.jwds666.fs.config.FileShareConfig;
import com.gitee.jwds666.fs.account.permission.PermissionResolver;

public abstract class AbstractEnvironment implements Environment{


    protected AccountManager accountManager;


    protected PermissionResolver permissionResolver;


    protected FileShareConfig config;

    public AbstractEnvironment(AccountManager accountManager, PermissionResolver permissionResolver, FileShareConfig config) {
        this.accountManager = accountManager;
        this.permissionResolver = permissionResolver;
        this.config = config;
    }

    public AbstractEnvironment() {
    }

    protected void assertObj(){
        if (accountManager == null)throw new RuntimeException("缺少账户管理器");
        if (permissionResolver == null)throw new RuntimeException("缺少权限解析器");
    }

    /**
     * 运行环境
     */
    @Override
    public void run() {
        assertObj();
        onRun();
    }
    /**
     * 运行环境
     */
    protected abstract void onRun();
}
