var utils = {
    newStack : function (){
        return {
            size:0,
            values:[],
            push:function (val){
                if (this.size == this.values.length){
                    this.values.push(val)
                }else{
                    this.values[this.size] = val
                }
                this.size++;
            },
            pop:function (){
                if (this.size != 0)return this.values[--this.size];
                return null;
            },
            clear:function (){
                this.values = []
                this.size = 0
            }
        }
    },
    mousePosition:function (ev){
        if(ev.pageX || ev.pageY){
            return {x:ev.pageX, y:ev.pageY};
        }
        return {
            x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
            y:ev.clientY + document.body.scrollTop - document.body.clientTop
        };
    },
    getData:function (res){
        if (res.code == 500){
            alert(res.msg)
        }else{
            return res.data;
        }
    },
    toggleClass: function (obj,remove,add){
        obj.removeClass(remove);
        obj.addClass(add);
    },
    invalidButtonClass: function (obj){
        this.toggleClass(obj,'file-button-valid','file-button-invalid');
    },
    validButtonClass:function (obj){
        this.toggleClass(obj,'file-button-invalid','file-button-valid');
    }
}