var styleClass = {
    init:function (){

    },
    tableTdAppend:function (tr,itemData){
        tr.append('<td class="file-name"><i class="fa fa-'+itemData.icon+'" ></i>'+itemData.name+'</td>')
        tr.append('<td class="file-attribute">'+itemData.updateTime+'</td>')
        tr.append('<td class="file-attribute">'+itemData.fileType+'</td>')
        var size = ""
        if (itemData.isFile){
            var z = itemData.size%1024 != 0 ? 1:0;
            size = parseInt(itemData.size/1024)+z+"kb"
        }
        tr.append('<td class="file-attribute">'+size+'</td>')
    }


}